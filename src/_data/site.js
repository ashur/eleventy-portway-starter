module.exports = {
	name: "Eleventy Portway Starter",
	description: "A template for building a simple blog with <a href=\"https://11ty.dev\">Eleventy</a> and <a href=\"https://getportway.com\">Portway</a>",
	url: "https://eleventy-portway-starter.netlify.app",
	author: {
		name: "Author Name",
		email: "author@example.com"
	}
};
