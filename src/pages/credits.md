---
title: Credits
---
This starter template looks a little jazzier thanks to art and photography by the following people.

## Icons

- Anchor, camera, ice cream, push pin, sunglasses, and whale by [Gia Hai]

[Gia Hai]: https://thenounproject.com/giahhai/

## Photos

- Field of green grass by [Bruno Martins]
- Melolonthinae on a banana leaf from [Wikimedia Commons]
- Watercolor paints by [Denise Johnson]
- White rabbit by [Buse Doga Ay]

[Bruno Martins]: https://unsplash.com/@brunus
[Denise Johnson]: https://unsplash.com/@auntneecey
[Buse Doga Ay]: https://unsplash.com/@busedoay
[Wikimedia Commons]: https://commons.wikimedia.org/wiki/Main_Page
